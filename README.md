# Your Poems
Aplikacja internetowa służąca dzieleniu się wierszami i innymi dziełaami literackimi tworzonymi przez jej użytkowników.

## Konfiguracja środowiska deweloperskiego
Po sklonowaniu repozytorium należy uruchomić skrypt:
```
./setup.sh
```

Uruchamianie usług:
```
./start-dev.sh
```
Synchronizacja bazy danych (po wcześniejszym uruchomieniu usług):
```
./sync-prisma.sh
```
Dostęp do bazy przez Prisma Studio: http://localhost:5555/

Adres aplikacji klienckiej:
```
http://localhost:5000
```

Adres aplikacji serwerowej:
```
http://localhost:5000/api
```

Plac zabaw dla GraphQL:
```
http://localhost:5000/api/graphql
```