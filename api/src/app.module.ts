import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { PhotonModule } from './photon/photon.module';
import { PoemsModule } from './poems/poems.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { CommentsModule } from './comments/comments.module';
import { GradesModule } from './grades/grades.module';
import { TagsModule } from './tags/tags.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      context: ({ req }) => ({ req }),
      debug: true,
      playground: true,
      autoSchemaFile: 'schema.gql',
      installSubscriptionHandlers: true,
      path: '/api/graphql',
    }),
    PoemsModule,
    PhotonModule,
    AuthModule,
    TagsModule,
    GradesModule,
    CommentsModule,
    UsersModule,
  ],
})
export class AppModule {}
