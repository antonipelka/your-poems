import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { CurrentUser } from 'src/decorators/current-user.decorator';
import { CurrentUserModel } from 'src/decorators/current-user.model';
import { GqlAuthGuard } from 'src/guards/gql.auth-guard';
import { AuthService } from './auth.service';
import { LoginUserInput } from './inputs/login-user.input';
import { TokenResponse } from './token.response';

@Resolver()
export class AuthResolver {
  constructor(
    private readonly authService: AuthService,
  ) {}

  @Mutation(() => TokenResponse)
  async login(@Args('data') payload: LoginUserInput) {
    return await this.authService.login(payload);
  }

  @Mutation(() => TokenResponse)
  @UseGuards(GqlAuthGuard)
  async refreshToken(@CurrentUser() user: CurrentUserModel) {
    return await this.authService.createTokenForUser(user);
  }
}
