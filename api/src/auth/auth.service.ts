import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import bcrypt from 'bcrypt';
import { CurrentUserModel } from 'src/decorators/current-user.model';
import { UserEntity } from 'src/users/user.entity';
import { UsersService } from '../users/users.service';
import { TokenResponse } from './token.response';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<UserEntity> {
    const user = await this.usersService.getUserByName(username);
    if (!user) {
      return null;
    }
    const result = await bcrypt.compare(pass, user.password);
    if (result) {
      return user;
    }
    return null;
  }

  async login(userCredentails: Record<'login' | 'password', string>) {
    const user = await this.validateUser(
      userCredentails.login,
      userCredentails.password,
    );
    if (!user) {
      throw new UnauthorizedException();
    }
    return this.createTokenForUser(user);
  }

  createTokenForUser({ id, name, role }: CurrentUserModel) {
    const payload = { id, name, role };

    return new TokenResponse({
      token: this.jwtService.sign(payload),
      user: payload,
    });
  }
}
