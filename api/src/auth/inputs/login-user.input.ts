import { Field, InputType } from "type-graphql";

@InputType()
export class LoginUserInput {
  @Field()
  login: string;

  @Field()
  password: string;
}