import { Field, ID, Int, ObjectType } from "type-graphql";
import { UserRole } from "src/users/enums/user-role";

@ObjectType()
export class AuthenticatedUser {
  @Field(() => ID)
  id: string;

  @Field()
  name: string;

  @Field()
  role: UserRole;
}

@ObjectType()
export class TokenResponse {
  @Field()
  token: string;

  @Field()
  user: AuthenticatedUser;

  constructor(data: TokenResponse) {
    Object.assign(this, data);
  }
}