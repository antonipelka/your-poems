import { ObjectType, Field, ID } from "type-graphql";
import { PoemEntity } from "src/poems/poem.entity";
import { UserEntity } from "src/users/user.entity";

@ObjectType()
export class CommentEntity {
  @Field(() => ID)
  id: string;

  @Field(type => UserEntity)
  author: UserEntity;

  @Field(type => PoemEntity)
  poem: PoemEntity;

  @Field()
  content: string;

  createdAt: Date;
  updatedAt: Date;

  constructor(data: any) {
    Object.assign(this, data);
  }
}