import { Module } from '@nestjs/common';
import { PhotonModule } from 'src/photon/photon.module';
import { CommentsRepository } from './comments.repository';
import { CommentsResolver } from './comments.resolver';
import { CommentsService } from './comments.service';

@Module({
  imports: [PhotonModule],
  providers: [
    CommentsRepository,
    CommentsResolver,
    CommentsService,
  ],
})
export class CommentsModule {}
