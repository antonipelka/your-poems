import { Injectable } from '@nestjs/common';
import { PhotonService } from 'src/photon/photon.service';
import { CommentEntity } from './comment.entity';
import { Comment } from '@prisma/photon';

@Injectable()
export class CommentsRepository {
  constructor(private readonly photon: PhotonService) {}

  async findById(commentId: string) {
    const comment = await this.photon.comments.findOne({
      where: {
        id: commentId,
      },
    });
    return mapToCommentEntity(comment);
  }

  async findByPoem(poemId: string) {
    const comments = await this.photon.comments.findMany({
      where: {
        poem: {
          id: poemId,
        },
      },
    });

    const mappedComments = comments.map(mapToCommentEntity);

    return mappedComments;
  }

  async findComments() {
    const comments = await this.photon.comments.findMany({
      include: {
        poem: true,
        author: true,
      },
    });

    const mappedComments = comments.map(mapToCommentEntity);

    return mappedComments;
  }

  async createComment(
    userId: string,
    poemId: string,
    { content }: CommentEntity,
  ) {
    const comment = await this.photon.comments.create({
      data: {
        content,
        author: {
          connect: { id: userId },
        },
        poem: {
          connect: { id: poemId },
        },
      },
      include: {
        author: true,
      },
    });

    return mapToCommentEntity(comment);
  }

  async updateComment({ id, content }: CommentEntity) {
    const existingComment = await this.findById(id);

    if (existingComment) {
      const updatedComment = await this.photon.comments.update({
        where: { id },
        data: {
          content,
        },
      });

      return mapToCommentEntity(updatedComment);
    } else {
      throw new Error(`Comment  with id: ${id} not found`);
    }
  }

  async deleteComment(commentId: string) {
    await this.photon.comments.delete({
      where: { id: commentId },
    });
  }

  async deleteCommentsByAuthor(authorId: string) {
    await this.photon.comments.deleteMany({
      where: { author: {id: authorId }},
    });
  }
}

const mapToCommentEntity = (data: Comment) =>
  data ? new CommentEntity(data) : null;
