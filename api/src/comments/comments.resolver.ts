import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { CommentsService } from './comments.service';
import { CreateCommentInput } from './inputs/create-comment.input';
import { CommentEntity } from './comment.entity';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from 'src/guards/gql.auth-guard';
import { CurrentUser } from 'src/decorators/current-user.decorator';
import { CurrentUserModel } from 'src/decorators/current-user.model';
import { UpdateCommentInput } from './inputs/update-comment.input';
import { DeleteCommentInput } from './inputs/delete-comment.input';

@Resolver()
export class CommentsResolver {
  constructor(private readonly commentsService: CommentsService) {}

  @Query(() => CommentEntity)
  async comment(@Args('id') tagId: string) {
    return await this.commentsService.getCommentById(tagId);
  }

  @Query(() => [CommentEntity])
  async comments() {
    return await this.commentsService.getComments();
  }

  @Query(() => [CommentEntity])
  async commentsByPoem(@Args('poemId') poemId: string) {
    return await this.commentsService.getCommentsByPoem(poemId);
  }

  @Mutation(() => CommentEntity)
  @UseGuards(GqlAuthGuard)
  async createComment(
    @CurrentUser() user: CurrentUserModel,
    @Args('data') { poemId, content }: CreateCommentInput,
  ) {
    const comment = await this.commentsService.createComment(user.id, poemId, {
      content,
    });

    return comment;
  }

  @Mutation(() => CommentEntity)
  @UseGuards(GqlAuthGuard)
  async updateComment(@Args('data') { id, content }: UpdateCommentInput) {
    const comment = await this.commentsService.updateComment({
      id,
      content,
    });

    return comment;
  }

  @Mutation(() => Boolean)
  @UseGuards(GqlAuthGuard)
  async deleteComment(@Args('data') { id }: DeleteCommentInput) {
    await this.commentsService.deleteComment(id);

    return true;
  }
}
