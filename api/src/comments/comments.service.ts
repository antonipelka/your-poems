import { Injectable } from '@nestjs/common';
import { CommentsRepository } from './comments.repository';
import { CommentEntity } from './comment.entity';

interface CreateComment {
  content: string;
}

@Injectable()
export class CommentsService {
  constructor(private readonly commentsRepository: CommentsRepository) {}

  async getCommentById(commentId: string) {
    return await this.commentsRepository.findById(commentId);
  }

  async getComments() {
    return await this.commentsRepository.findComments();
  }

  async getCommentsByPoem(commentId: string) {
    return await this.commentsRepository.findByPoem(commentId);
  }

  async createComment(userId: string, poemId: string, { content }: CreateComment) {
    const commentEntity = new CommentEntity({
      content,
    });

    return await this.commentsRepository.createComment(userId, poemId, commentEntity);
  }

  async updateComment({ id, content }) {
    const commentEntity = new CommentEntity({
      id,
      content,
    });
    return await this.commentsRepository.updateComment(commentEntity);
  }

  async deleteComment(commentId: string) {
    return await this.commentsRepository.deleteComment(commentId);
  }

  async deleteCommentsByAuthor(authorId: string){
    return await this.commentsRepository.deleteCommentsByAuthor(authorId);
  }
}
