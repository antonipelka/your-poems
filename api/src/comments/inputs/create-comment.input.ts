import { IsString, MaxLength, MinLength } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class CreateCommentInput {
  @Field()
  @IsString()
  poemId: string;

  @Field()
  @IsString()
  @MinLength(5)
  @MaxLength(200)
  content: string;
}
