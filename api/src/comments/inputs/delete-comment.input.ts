import { InputType, Field } from 'type-graphql';
import { IsEmail, IsString, MinLength, MaxLength } from 'class-validator';

@InputType()
export class DeleteCommentInput {
  @Field()
  @IsString()
  id: string;
}
