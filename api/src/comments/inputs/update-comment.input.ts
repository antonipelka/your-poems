import { InputType, Field } from 'type-graphql';
import { IsEmail, IsString, MinLength, MaxLength } from 'class-validator';

@InputType()
export class UpdateCommentInput {
  @Field()
  @IsString()
  @MinLength(10)
  @MaxLength(200)
  id: string;

  @Field()
  @IsString()
  @MinLength(10)
  @MaxLength(200)
  content: string;
}
