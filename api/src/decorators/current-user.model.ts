import { UserRole } from "src/users/enums/user-role";

export class CurrentUserModel {
    id: string;
    name: string;
    role: UserRole;
}