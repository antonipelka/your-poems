import { ObjectType, Field, ID } from "type-graphql";
import { UserEntity } from "src/users/user.entity";

@ObjectType()
export class GradeEntity {
  @Field(() => ID)
  id: string;

  @Field()
  like: boolean;

  @Field()
  author: UserEntity;

  createdAt: Date;
  updatedAt: Date;

  constructor(data: any) {
    Object.assign(this, data);
  }
}