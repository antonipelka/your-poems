import { Module } from '@nestjs/common';
import { PhotonModule } from 'src/photon/photon.module';
import { PoemsRepository } from 'src/poems/poems.repository';
import { GradesRepository } from './grades.repository';
import { GradesResolver } from './grades.resolver';
import { GradesService } from './grades.service';

const externalProviders = [
  PoemsRepository,
];

@Module({
  imports: [PhotonModule],
  providers: [
    GradesRepository,
    GradesResolver,
    GradesService,
    ...externalProviders,
  ],
})
export class GradesModule {}
