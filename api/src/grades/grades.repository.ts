import { Injectable } from '@nestjs/common';
import { PhotonService } from 'src/photon/photon.service';
import { GradeEntity } from './grade.entity';
import { Grade } from '@prisma/photon';
import { LikeFragmentEntity } from './like-fragment.entity';

@Injectable()
export class GradesRepository {
  constructor(private readonly photon: PhotonService) {}

  async findById(gradeId: string) {
    const grade = await this.photon.grades.findOne({
      where: {
        id: gradeId,
      },
    });
    return mapToGradeEntity(grade);
  }

  async findByPoemAndAuthor(poemId: string, userId: string) {
    const grades = await this.photon.grades.findMany({
      where: {
        author: {
          id: userId,
        },
        poem: {
          id: poemId,
        },
      },
    });

    return grades.map(mapToGradeEntity);
  }

  async findByPoem(poemId: string) {
    const grades = await this.photon.grades.findMany({
      where: {
        poem: {
          id: poemId,
        },
      },
    });

    return grades.map(mapToGradeEntity);
  }

  async findByPoemAndUser(poemId: string, userId: string) {
    const grade = await this.photon.grades.findMany({
      where: {
        poem: { id: poemId },
        author: {
          id: userId,
        },
      },
    });

    return mapToGradeEntity(grade[0]);
  }

  async findGrades() {
    const grades = await this.photon.grades.findMany({
      include: {
        author: false,
      },
    });

    return grades.map(mapToGradeEntity);
  }

  async toggleLike(poemId: string, userId: string) {
    let newLikesCount = 0;
    let liked = false;
    const existing = await this.findByPoemAndUser(poemId, userId);
    if (existing) {
      await this.deleteGrade(existing.id);
      const { likesCount } = await this.photon.poems.findOne({
        where: { id: poemId },
      });
      newLikesCount = likesCount - 1;
      await this.photon.poems.update({
        where: {
          id: poemId,
        },
        data: {
          likesCount: newLikesCount,
        }
      })
    } else {
      liked = true;
      await this.createGrade(userId, poemId);
      const { likesCount } = await this.photon.poems.findOne({
        where: { id: poemId },
      });
      newLikesCount = likesCount + 1;
      await this.photon.poems.update({
        where: {
          id: poemId,
        },
        data: {
          likesCount: newLikesCount,
        }
      })
    }

    return new LikeFragmentEntity({
      poemId,
      liked,
      likesCount: newLikesCount,
    });
  }

  async createGrade(userId: string, poemId: string) {
    const existingGradeForPoem = await this.findByPoemAndUser(poemId, userId);
    if (existingGradeForPoem) {
      throw new Error(`Poem was graded by this user`);
    }

    const grade = await this.photon.grades.create({
      data: {
        like: true,
        author: {
          connect: { id: userId },
        },
        poem: {
          connect: { id: poemId },
        },
      },
    });

    return mapToGradeEntity(grade);
  }

  async updateGrade({ id, like }: GradeEntity) {
    const existingGrade = await this.findById(id);

    if (existingGrade) {
      const updatedGrade = await this.photon.grades.update({
        where: { id },
        data: {
          like,
        },
      });

      return mapToGradeEntity(updatedGrade);
    } else {
      throw new Error(`Grade  with id: ${id} not found`);
    }
  }

  async deleteGrade(gradeId: string) {
    await this.photon.grades.delete({
      where: { id: gradeId },
    });
  }

  async deleteGradesByAuthor(authorId: string) {
    await this.photon.grades.deleteMany({
      where: { author: { id: authorId } },
    });
  }
}

const mapToGradeEntity = (data: Grade) => (data ? new GradeEntity(data) : null);
