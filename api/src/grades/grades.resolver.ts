import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { CurrentUser } from 'src/decorators/current-user.decorator';
import { CurrentUserModel } from 'src/decorators/current-user.model';
import { GqlAuthGuard } from 'src/guards/gql.auth-guard';
import { PoemsRepository, Like } from 'src/poems/poems.repository';
import { GradeEntity } from './grade.entity';
import { GradesService } from './grades.service';
import { CreateGradeInput } from './inputs/create-grade.input';
import { DeleteGradeInput } from './inputs/delete-grade.input';
import { UpdateGradeInput } from './inputs/update-grade.input';
import { LikeFragmentEntity } from './like-fragment.entity';
import { GradesRepository } from './grades.repository';

@Resolver()
export class GradesResolver {
  constructor(
    private readonly gradesService: GradesService,
    private readonly gradesRepository: GradesRepository,
    private readonly poemsRepository: PoemsRepository,
  ) {}

  @Query(() => GradeEntity)
  async grade(@Args('id') tagId: string) {
    return await this.gradesService.getGradeById(tagId);
  }

  @Query(() => [GradeEntity])
  async grades() {
    return await this.gradesService.getGrades();
  }

  @Mutation(() => LikeFragmentEntity)
  @UseGuards(GqlAuthGuard)
  async toggleLike(
    @CurrentUser() user: CurrentUserModel,
    @Args('poemId') poemId: string,
  ) {
    return await this.gradesRepository.toggleLike(poemId, user.id);
  }

  @Mutation(() => GradeEntity)
  @UseGuards(GqlAuthGuard)
  async createGrade(
    @CurrentUser() user: CurrentUserModel,
    @Args('data') { poemId, like }: CreateGradeInput,
  ) {
    const grade = await this.gradesService.createGrade(user.id, poemId);

    await this.poemsRepository.updatePoemLikesCount(poemId, Like.Add);

    return grade;
  }

  @Mutation(() => GradeEntity)
  @UseGuards(GqlAuthGuard)
  async updateGrade(@Args('data') { id, like }: UpdateGradeInput) {
    const grade = await this.gradesService.updateGrade({
      id,
      like,
    });

    await this.poemsRepository.updatePoemLikesCount(
      id,
      like ? Like.Add : Like.Remove,
    );

    return grade;
  }

  @Mutation(() => Boolean)
  @UseGuards(GqlAuthGuard)
  async deleteGrade(@Args('data') { id, poemId }: DeleteGradeInput) {
    await this.gradesService.deleteGrade(id);
    await this.poemsRepository.updatePoemLikesCount(poemId, Like.Remove);
    return true;
  }
}
