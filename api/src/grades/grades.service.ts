import { Injectable } from '@nestjs/common';
import { GradesRepository } from './grades.repository';
import { GradeEntity } from './grade.entity';

interface CreateGrade {
  like: boolean;
}

@Injectable()
export class GradesService {
  constructor(private readonly gradesRepository: GradesRepository) {}

  async getGradeById(tagId: string) {
    return await this.gradesRepository.findById(tagId);
  }

  async createGrade(userId: string, poemId: string) {

    return await this.gradesRepository.createGrade(userId, poemId);
  }

  async getGrades() {
    return await this.gradesRepository.findGrades();
  }

  async getGradesByPoemAndAuthor(userId: string, poemId: string) {
    return await this.gradesRepository.findByPoemAndAuthor(userId, poemId);
  }

  async getGradesByPoem(poemId: string) {
    return await this.gradesRepository.findByPoem(poemId);
  }

  async updateGrade({ id, like }) {
    const gradeEntity = new GradeEntity({
      id,
      like,
    });
    return await this.gradesRepository.updateGrade(gradeEntity);
  }

  async deleteGrade(gradeId: string) {
    return await this.gradesRepository.deleteGrade(gradeId);
  }

  async deleteGradesByAuthor(authorId: string) {
    return await this.gradesRepository.deleteGradesByAuthor(authorId);
  }
}
