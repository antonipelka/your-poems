import { InputType, Field } from 'type-graphql';
import { IsString, MinLength, MaxLength } from 'class-validator';

@InputType()
export class CreateGradeInput {
  @Field()
  like: boolean;

  @Field()
  @IsString()
  poemId: string;
}
