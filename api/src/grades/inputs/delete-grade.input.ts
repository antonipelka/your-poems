import { InputType, Field } from 'type-graphql';
import { IsEmail, IsString, MinLength, MaxLength } from 'class-validator';

@InputType()
export class DeleteGradeInput {
  @Field()
  @IsString()
  id: string;
  
  @Field()
  @IsString()
  poemId: string
}
