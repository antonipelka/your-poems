import { InputType, Field } from 'type-graphql';
import {
  IsEmail,
  IsString,
  MinLength,
  MaxLength,
  IsBoolean,
} from 'class-validator';

@InputType()
export class UpdateGradeInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsBoolean()
  like: boolean;
}
