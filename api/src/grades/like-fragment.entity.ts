import { Field, ID, ObjectType } from "type-graphql";

@ObjectType()
export class LikeFragmentEntity {
  @Field(() => ID)
  poemId: string;

  @Field()
  liked: boolean;

  @Field()
  likesCount: number;

  constructor(data: any) {
    Object.assign(this, data);
  }
}