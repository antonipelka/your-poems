import { Injectable } from '@nestjs/common';
import { Photon } from '@prisma/photon';

@Injectable()
export class PhotonService extends Photon {}
