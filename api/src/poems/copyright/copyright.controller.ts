import { Controller, Get } from '@nestjs/common';
import { CopyrightService } from './copyright.service';

@Controller('copyright')
export class CopyrightController {
  constructor(
    private readonly copyrightService: CopyrightService,
  ) {}

  @Get('data')
  async getCopyrightData() {
    return await this.copyrightService.getTrainingData();
  }
}
