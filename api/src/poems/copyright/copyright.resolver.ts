import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { CopyrightedPoemEntity } from './copyrighted-poem.entity';
import { CopyrightService } from './copyright.service';

@Resolver()
export class CopyrightResolver {
  constructor(private readonly copyrightService: CopyrightService) {}

  @Query(() => [CopyrightedPoemEntity])
  async copyrightedPoems() {
    return await this.copyrightService.getCopyrightedPoems();
  }

  @Mutation(() => Boolean)
  async addCopyrightedPoem(@Args('content') content: string) {
    await this.copyrightService.addCopyrightedPoem(content);

    return true;
  }

  @Mutation(() => Boolean)
  trainCopyrightModel() {
    this.copyrightService.trainModel();

    return true;
  }

  @Query(() => Boolean)
  async checkCopyright(@Args('content') content: string) {
    return await this.copyrightService.checkCopyright(content);
  }
}
