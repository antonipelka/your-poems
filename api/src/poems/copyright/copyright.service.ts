import { Injectable } from '@nestjs/common';
import Axios from 'axios';
import { PhotonService } from 'src/photon/photon.service';
import { CopyrightedPoemEntity } from './copyrighted-poem.entity';
import { CopyrightedPoem } from '@prisma/photon';

const COPYRIGHT_GUARDIAN_HOST = 'http://copyright-guardian';

@Injectable()
export class CopyrightService {
  constructor(
    private readonly photon: PhotonService,
  ) {}

  async getCopyrightedPoems() {
    const poems = await this.photon.copyrightedPoems.findMany();

    return poems.map(mapToCopyrightedPoemEntity)
  }

  async addCopyrightedPoem(content: string) {
    await this.photon.copyrightedPoems.create({ data: { content } });
  }

  async getTrainingData() {
    const userPoems = await this.photon.poems.findMany({
      select: {
        content: true,
      },
    });
    const userTexts = userPoems.map(({ content }) => content);
    const copyrightedPoems = await this.photon.copyrightedPoems.findMany();
    const copyrightedTexts = copyrightedPoems.map(({ content }) => content);

    return [
      ...userTexts,
      ...copyrightedTexts,
    ];
  }

  trainModel() {
    Axios.post(`${COPYRIGHT_GUARDIAN_HOST}/train`);
  }

  async checkCopyright(content: string) {
    const response = await Axios.post(
      `${COPYRIGHT_GUARDIAN_HOST}/check`,
      { content },
      {
        headers: { 'Content-Type': 'application/json' },
      })

    return response.data.copyright;
  }
}

const mapToCopyrightedPoemEntity = (data: CopyrightedPoem) => (data ? new CopyrightedPoemEntity(data) : null);
