import { ObjectType, Field, ID } from "type-graphql";

@ObjectType()
export class CopyrightedPoemEntity {
  @Field(() => ID)
  id: string;

  @Field()
  content: string;

  constructor(data: any) {
    Object.assign(this, data);
  }
}