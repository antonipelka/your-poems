import { IsArray, IsOptional, IsString, MaxLength, MinLength } from "class-validator";
import { Field, InputType } from "type-graphql";

@InputType()
export class CreatePoemInput {

  @Field()
  @IsString()
  @MinLength(2)
  @MaxLength(40)
  title: string;

  @Field()
  @IsString()
  @MinLength(20)
  content: string;

  @Field(() => [String])
  @IsOptional()
  @IsArray()
  tags: string[];
}