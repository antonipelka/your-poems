import { InputType, Field } from 'type-graphql';
import { IsEmail, IsString, MinLength, MaxLength } from 'class-validator';

@InputType()
export class DeletePoemInput {
  @Field()
  @IsString()
  id: string;
}
