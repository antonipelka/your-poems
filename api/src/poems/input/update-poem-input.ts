import { InputType, Field } from 'type-graphql';
import { IsEmail, IsString, MinLength, MaxLength, IsArray, IsOptional } from 'class-validator';

@InputType()
export class UpdatePoemInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsOptional()
  @IsString()
  title: string;

  @Field()
  @IsOptional()
  @IsString()
  @MinLength(20)
  content: string;

  @Field(() => [String])
  @IsOptional()
  @IsArray()
  tags: string[];
}
