import { CommentEntity } from "src/comments/comment.entity";
import { GradeEntity } from "src/grades/grade.entity";
import { TagEntity } from "src/tags/tag.entity";
import { UserEntity } from "src/users/user.entity";
import { Field, ID, ObjectType } from "type-graphql";

@ObjectType()
export class PoemEntity {
  @Field(() => ID)
  id: string;

  @Field()
  title: string;

  @Field()
  content: string;

  @Field()
  author: UserEntity;

  @Field(type => [CommentEntity], { nullable: true })
  comments?: CommentEntity[];

  @Field(type => [TagEntity], { nullable: true })
  tags?: TagEntity[];

  @Field(type => [GradeEntity], { nullable: true })
  grades?: GradeEntity[];

  @Field()
  likesCount: number;

  @Field()
  liked: boolean;

  @Field()
  createdAt: Date;
  updatedAt: Date;

  constructor(data: any) {
    Object.assign(this, data);
  }
}