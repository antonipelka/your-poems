import { Module } from '@nestjs/common';
import { PhotonModule } from 'src/photon/photon.module';
import { CopyrightController } from './copyright/copyright.controller';
import { CopyrightResolver } from './copyright/copyright.resolver';
import { CopyrightService } from './copyright/copyright.service';
import { PoemsRepository } from './poems.repository';
import { PoemsResolver } from './poems.resolver';
import { PoemsService } from './poems.service';

@Module({
  imports: [PhotonModule],
  providers: [
    PoemsResolver,
    PoemsService,
    PoemsRepository,
    CopyrightService,
    CopyrightResolver,
  ],
  controllers: [CopyrightController],
  exports: [
    PoemsRepository,
  ],
})
export class PoemsModule {}
