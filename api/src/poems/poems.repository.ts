import { Injectable } from '@nestjs/common';
import { PhotonService } from 'src/photon/photon.service';
import { Poem } from '@prisma/photon';
import { PoemEntity } from './poem.entity';

export enum Like {
  Add = 1,
  Remove = -1,
}

@Injectable()
export class PoemsRepository {
  constructor(private readonly photon: PhotonService) { }

  async findById(poemId: string) {
    const poem = await this.photon.poems.findOne({
      where: {
        id: poemId,
      },
      include: {
        author: true,
        comments: {
          include: {
            author: true,
          },
        },
        tags: true,
        grades: {
          include: {
            author: true,
          },
        },
      },
    });
    return mapToPoemEntity(poem);
  }

  async findPoems() {
    const poems = await this.photon.poems.findMany({
      orderBy: {
        createdAt: 'desc',
      },
      include: {
        author: true,
        comments: {
          include: {
            author: true
          }
        },
        tags: true,
        grades: {
          include: {
            author: true,
          },
        },
      },
    });

    return poems.map(mapToPoemEntity);
  }

  async findByAuthor(userId: string) {
    const poems = await this.photon.poems.findMany({
      where: {
        author: { id: userId },
      },
      include: {
        author: true,
        comments: {
          include: {
            author: true,
          },
        },
        tags: true,
        grades: {
          include: {
            author: true,
          },
        },
      },
    });

    return poems.map(mapToPoemEntity);
  }

  async findByTitle(title: string) {
    const poem = await this.photon.poems.findOne({
      where: {
        title,
      },
    });

    return mapToPoemEntity(poem);
  }

  async findByTags(tags: string[]) {
    const poems = await this.photon.poems.findMany({
      where: {
        tags: {
          some: {
            OR: tags.map(tag => ({ name: tag })),
          },
        },
      },
      include: {
        author: true,
        comments: {
          include: {
            author: true,
          },
        },
        tags: true,
        grades: {
          include: {
            author: true,
          },
        },
      },
    });

    return poems.map(mapToPoemEntity);
  }

  async upsertTags(userId: string, tags: string[]) {
    for (let tag of tags) {
      await this.photon.tags.upsert({
        where: {
          name: tag,
        },
        create: {
          author: {
            connect: {
              id: userId,
            }
          },
          name: tag,
        },
        update: {},
      });
    }
  }

  async createPoem(
    userId: string,
    tags: string[],
    { title, content }: PoemEntity,
  ) {
    const existingPoemWithTitle = await this.findByTitle(title);
    if (existingPoemWithTitle) {
      throw new Error(`Title ${title} is already in use`);
    }
    const likesCount = 0;
    await this.upsertTags(userId, tags);
    const poem = await this.photon.poems.create({
      data: {
        title,
        content,
        likesCount,
        author: {
          connect: { id: userId },
        },
        tags: {
          connect: tags.map((tag) => ({ name: tag.toLowerCase() })),
        },
      },
    });

    return mapToPoemEntity(poem);
  }

  async updatePoem(
    tags: string[],
    { id, title, content, likesCount }: PoemEntity,
  ) {
    const existingPoem = await this.findById(id);

    if (existingPoem) {
      await this.upsertTags(existingPoem.author.id, tags);
      const updatedPoem = await this.photon.poems.update({
        where: { id },
        data: {
          title,
          content,
          likesCount,
          tags: {
            set: tags.map((tag) => ({ name: tag })),
          },
        },
      });

      return mapToPoemEntity(updatedPoem);
    } else {
      throw new Error(`Poem with id: ${id} not found`);
    }
  }

  async updatePoemLikesCount(id: string, like: Like) {
    const { likesCount } = await this.photon.poems.findOne({ where: { id } });
    const { likesCount: newLikesCount } = await this.photon.poems.update({
      where: { id },
      data: {
        likesCount: likesCount + like,
      },
    });

    return newLikesCount;
  }

  async deletePoem(poemId: string) {
    await this.photon.comments.deleteMany({
      where: {
        poem: {
          id: poemId,
        },
      },
    })
    await this.photon.grades.deleteMany({
      where: {
        poem: {
          id: poemId,
        },
      },
    })
    await this.photon.poems.delete({
      where: { id: poemId },
    });
  }

  async deletePoemsByAuthor(authorId: string) {
    await this.photon.poems.deleteMany({
      where: { author: { id: authorId } },
    });
  }

  async getTopPoems() {
    const poems = await this.photon.poems.findMany({
      orderBy: {
        likesCount: 'desc',
      },
      last: 5,
      include: {
        author: true,
        comments: {
          include: {
            author: true
          }
        },
        tags: true,
        grades: {
          include: {
            author: true,
          },
        },
      },
    });

    return poems.map(mapToPoemEntity);
  }
}

const mapToPoemEntity = (data: Poem) => (data ? new PoemEntity(data) : null);
