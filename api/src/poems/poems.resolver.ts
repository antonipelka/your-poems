import { UseGuards } from '@nestjs/common';
import {
  Args,
  Mutation,
  Query,
  Resolver,
  ResolveProperty,
} from '@nestjs/graphql';
import { PoemsService } from './poems.service';
import { CreatePoemInput } from './input/create-poem.input';
import { GqlAuthGuard } from 'src/guards/gql.auth-guard';
import { PoemEntity } from './poem.entity';
import { CurrentUser } from 'src/decorators/current-user.decorator';
import { CurrentUserModel } from 'src/decorators/current-user.model';
import { UpdatePoemInput } from './input/update-poem-input';
import { DeletePoemInput } from './input/delete-poem.input';
import { PoemsRepository } from './poems.repository';

@Resolver()
export class PoemsResolver {
  constructor(
    private readonly poemsService: PoemsService,
    private readonly poemsRepository: PoemsRepository,
  ) { }

  @Query(() => [PoemEntity])
  async poems() {
    return await this.poemsService.getPoems();
  }

  @Query(() => [PoemEntity])
  async topPoems() {
    return await this.poemsRepository.getTopPoems();
  }

  @Query(() => [PoemEntity])
  async searchPoems(@Args('query') query: string) {
    return await this.poemsService.search(query);
  }

  @Query(() => PoemEntity)
  async getPoem(@Args('id') id: string) {
    return await this.poemsService.getPoemById(id);
  }

  @Query(() => [PoemEntity])
  @UseGuards(GqlAuthGuard)
  async poemsOfLoggedUser(@CurrentUser() user: CurrentUserModel) {
    return await this.poemsService.getPoemsByAuthor(user.id);
  }

  @Mutation(() => PoemEntity)
  @UseGuards(GqlAuthGuard)
  async createPoem(
    @CurrentUser() user: CurrentUserModel,
    @Args('data') { title, content, tags }: CreatePoemInput,
  ) {
    const poem = await this.poemsService.createPoem(user.id, {
      title,
      content,
      tags,
    });

    return poem;
  }

  @Mutation(() => PoemEntity)
  @UseGuards(GqlAuthGuard)
  async updatePoem(@Args('data') { id, title, content, tags }: UpdatePoemInput) {
    const poem = await this.poemsService.updatePoem({
      id,
      title,
      content,
      tags,
    });

    return poem;
  }

  @Mutation(() => Boolean)
  @UseGuards(GqlAuthGuard)
  async deletePoem(@Args('data') { id }: DeletePoemInput) {
    await this.poemsService.deletePoem(id);

    return true;
  }
}
