import { Injectable, BadRequestException } from '@nestjs/common';
import { PoemsRepository, Like } from './poems.repository';
import { PoemEntity } from './poem.entity';
import { CopyrightService } from './copyright/copyright.service';

interface CreatePoem {
  title: string;
  content: string;
  tags: string[];
}

@Injectable()
export class PoemsService {
  constructor(
    private readonly copyrightService: CopyrightService,
    private readonly poemsRepository: PoemsRepository,
  ) {}

  async getPoemById(poemId: string) {
    return await this.poemsRepository.findById(poemId);
  }

  async getPoems() {
    return await this.poemsRepository.findPoems();
  }

  async search(query: string) {
    const tags = query.split(',').map((tag) => tag.trim());

    return await this.poemsRepository.findByTags(tags);
  }

  async getPoemsByAuthor(userId: string) {
    return await this.poemsRepository.findByAuthor(userId);
  }

  async createPoem(userId: string, { title, content, tags }: CreatePoem) {
    const isCopyrighted = await this.copyrightService.checkCopyright(content);
    if (isCopyrighted) {
      throw new BadRequestException('Copyright infringement!');
    }

    const poemEntity = new PoemEntity({
      title,
      content,
    });

    return await this.poemsRepository.createPoem(userId, tags, poemEntity);
  }

  async updatePoem({ id, title, content, tags }) {
    const poemEntity = new PoemEntity({
      id,
      title,
      content,
    });
    return await this.poemsRepository.updatePoem(tags, poemEntity);
  }

  async updatePoemLikesCount(id: string, like: boolean) {
    return await this.poemsRepository.updatePoemLikesCount(id, like ? Like.Add : Like.Remove);
  }

  async deletePoem(poemId: string) {
    return await this.poemsRepository.deletePoem(poemId);
  }

  async deletePoemsByAuthor(authorId: string) {
    return await this.poemsRepository.deletePoemsByAuthor(authorId);
  }
}
