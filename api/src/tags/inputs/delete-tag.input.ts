import { InputType, Field } from 'type-graphql';
import { IsString, MinLength, MaxLength } from 'class-validator';

@InputType()
export class DeleteTagInput {
  @Field()
  @IsString()
  id: string;
}
