import { InputType, Field } from 'type-graphql';
import { IsString, MinLength, MaxLength } from 'class-validator';

@InputType()
export class UpdateTagInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  @MinLength(2)
  @MaxLength(30)
  name: string;
}
