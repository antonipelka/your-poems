import { ObjectType, Field, ID } from "type-graphql";

@ObjectType()
export class TagEntity {
  @Field(() => ID)
  id: string;

  @Field()
  name: string;

  createdAt: Date;
  updatedAt: Date;

  constructor(data: any) {
    Object.assign(this, data);
  }
}