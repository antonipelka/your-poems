import { Module } from '@nestjs/common';
import { PhotonModule } from 'src/photon/photon.module';
import { TagsService } from './tags.service';
import { TagsResolver } from './tags.resolver';
import { TagsRepository } from './tags.repository';

@Module({
  imports: [PhotonModule],
  providers: [
    TagsService,
    TagsResolver,
    TagsRepository,
  ],
})
export class TagsModule {}
