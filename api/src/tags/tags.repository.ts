import { Injectable } from '@nestjs/common';
import { PhotonService } from 'src/photon/photon.service';
import { TagEntity } from './tag.entity';
import { Tag } from '@prisma/photon';

@Injectable()
export class TagsRepository {
  constructor(private readonly photon: PhotonService) {}

  async findById(tagId: string) {
    const tag = await this.photon.tags.findOne({
      where: {
        id: tagId,
      },
    });
    return mapToTagEntity(tag);
  }

  async findByName(nameToFind: string) {
    const tag = await this.photon.tags.findOne({
      where: {
        name: nameToFind,
      },
    });

    return mapToTagEntity(tag);
  }

  async findTags() {
    const tags = await this.photon.tags.findMany({
      include: {
        author: true,
      },
    });

    const mappedTags = tags.map(mapToTagEntity);

    return mappedTags;
  }

  // async findByPoem(poemId: string) {
  //   const tags = await this.photon.tags.findMany({
  //     where: {
  //       poem: {
  //         id: poemId,
  //       },
  //     },
  //   });

  //   const mappedTags = tags.map(mapToTagEntity);

  //   return mappedTags;
  // }

  async createTag(userId: string, { name }: TagEntity) {
    const existingTagWithName = await this.findByName(name);
    if (existingTagWithName) {
      throw new Error(`Name ${name} is already in use`);
    }
    const tag = await this.photon.tags.create({
      data: {
        name,
        author: {
          connect: { id: userId },
        },
      },
    });

    return mapToTagEntity(tag);
  }

  async updateTag({ id, name }: TagEntity) {
    const existingTag = await this.findById(id);

    if (existingTag) {
      const updatedTag = await this.photon.tags.update({
        where: { id },
        data: {
          name,
        },
      });

      return mapToTagEntity(updatedTag);
    } else {
      throw new Error(`Poem with id: ${id} not found`);
    }
  }

  async deleteTag(tagId: string) {
    await this.photon.tags.delete({
      where: { id: tagId },
    });
  }

  async deleteTagsByAuthor(authorId: string) {
    await this.photon.tags.deleteMany({
      where: { author: { id: authorId } },
    });
  }
}

const mapToTagEntity = (data: Tag) => (data ? new TagEntity(data) : null);
