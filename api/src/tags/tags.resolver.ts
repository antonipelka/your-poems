import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { TagsService } from './tags.service';
import { TagEntity } from './tag.entity';
import { CreateTagInput } from './inputs/create-tag.input';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from 'src/guards/gql.auth-guard';
import { CurrentUser } from 'src/decorators/current-user.decorator';
import { CurrentUserModel } from 'src/decorators/current-user.model';
import { UpdateTagInput } from './inputs/update-tag.input';
import { DeletePoemInput } from 'src/poems/input/delete-poem.input';
import { Roles } from 'src/decorators/roles.decorator';
import { UserRole } from 'src/users/enums/user-role';
import { RolesGuard } from 'src/guards/gql.role-guard';

@Resolver()
export class TagsResolver {
  constructor(private readonly tagsService: TagsService) {}

  @Query(() => TagEntity)
  async tag(@Args('id') tagId: string) {
    return await this.tagsService.getTagById(tagId);
  }

  @Query(() => [TagEntity])
  async tags() {
    return await this.tagsService.getTags();
  }

  @Mutation(() => TagEntity)
  @UseGuards(GqlAuthGuard)
  async createTag(
    @CurrentUser() user: CurrentUserModel,
    @Args('data') { name }: CreateTagInput,
  ) {
    const tag = await this.tagsService.createTag(user.id, { name });

    return tag;
  }

  @Mutation(() => TagEntity)
  @UseGuards(GqlAuthGuard)
  async updateTag(@Args('data') { id, name }: UpdateTagInput) {
    const tag = await this.tagsService.updateTag({
      id,
      name,
    });

    return tag;
  }

  @Mutation(() => Boolean)
  @UseGuards(GqlAuthGuard, RolesGuard)
  @Roles(UserRole.ADMIN)
  async deleteTag(@Args('data') { id }: DeletePoemInput) {
    await this.tagsService.deleteTag(id);

    return true;
  }
}
