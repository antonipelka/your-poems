import { Injectable } from '@nestjs/common';
import { TagsRepository } from './tags.repository';
import { TagEntity } from './tag.entity';

interface CreateTag {
  name: string;
}

@Injectable()
export class TagsService {
  constructor(private readonly tagsRepository: TagsRepository) {}

  async getTagById(tagId: string) {
    return await this.tagsRepository.findById(tagId);
  }

  async getTags() {
    return await this.tagsRepository.findTags();
  }

  async createTag(userId: string, { name }: CreateTag) {
    const tagEntity = new TagEntity({
      name,
    });
    return await this.tagsRepository.createTag(userId, tagEntity);
  }

  async updateTag({ id, name }) {
    const tagEntity = new TagEntity({
      id,
      name,
    });

    return await this.tagsRepository.updateTag(tagEntity);
  }

  async deleteTag(tagId: string) {
    return await this.tagsRepository.deleteTag(tagId);
  }

  async deleteTagsByAuthor(authorId: string){
    return await this.tagsRepository.deleteTagsByAuthor(authorId);
  }
}
