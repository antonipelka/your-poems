import { InputType, Field } from 'type-graphql';
import { IsEmail, IsString, MinLength, MaxLength } from 'class-validator';

@InputType()
export class DeleteUserInput {
  @Field()
  @IsString()
  id: string;
}
