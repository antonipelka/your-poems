import { InputType, Field } from "type-graphql";
import { IsEmail, IsString, MinLength, MaxLength } from "class-validator";

@InputType()
export class RegisterUserInput {
  @Field()
  @IsEmail()
  email: string;

  @Field()
  @IsString()
  @MinLength(2)
  @MaxLength(30)
  name: string;

  @Field()
  @IsString()
  @MinLength(6)
  password: string;
}