import { InputType, Field } from 'type-graphql';
import { IsEmail, IsString, MinLength, MaxLength } from 'class-validator';
import { UserRole } from '../enums/user-role';

@InputType()
export class UpdateUserInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsEmail()
  email: string;

  @Field()
  @IsString()
  @MinLength(2)
  @MaxLength(30)
  name: string;

  @Field()
  @IsString()
  @MinLength(6)
  password: string;

  @Field()
  role: UserRole;
}
