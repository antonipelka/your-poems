import { ObjectType, Field, ID } from "type-graphql";
import { UserRole } from "./enums/user-role";

@ObjectType()
export class UserEntity {
  @Field(() => ID)
  id: string;

  @Field()
  name: string;

  @Field()
  email: string;

  password: string;

  role: UserRole = UserRole.USER;

  createdAt: Date;
  updatedAt: Date;

  constructor(data: any) {
    Object.assign(this, data);
  }
}