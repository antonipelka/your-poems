import { Module } from '@nestjs/common';
import { CommentsRepository } from 'src/comments/comments.repository';
import { GradesRepository } from 'src/grades/grades.repository';
import { PhotonModule } from 'src/photon/photon.module';
import { PoemsRepository } from 'src/poems/poems.repository';
import { TagsRepository } from 'src/tags/tags.repository';
import { UsersRepository } from './users.repository';
import { UsersResolver } from './users.resolver';
import { UsersService } from './users.service';
import { PoemsModule } from 'src/poems/poems.module';

const externalProviders = [
  CommentsRepository,
  GradesRepository,
  PoemsRepository,
  TagsRepository,
];

@Module({
  imports: [
    PhotonModule,
    PoemsModule,
  ],
  providers: [
    UsersService,
    UsersResolver,
    UsersRepository,
    ...externalProviders,
  ],
  exports: [UsersService],
})
export class UsersModule {}
