import { Injectable } from '@nestjs/common';
import { PhotonService } from 'src/photon/photon.service';
import { UserEntity } from './user.entity';
import { User } from '@prisma/photon';
import { UserRole } from './enums/user-role';

@Injectable()
export class UsersRepository {
  constructor(private readonly photon: PhotonService) {}

  async findById(userId: string) {
    const user = await this.photon.users.findOne({
      where: {
        id: userId,
      },
    });

    return mapToUserEntity(user);
  }

  async findUsers() {
    const users = await this.photon.users.findMany({});

    const mappedUsers = [];

    users.forEach(item => {
      mappedUsers.push(mapToUserEntity(item));
    });

    return mappedUsers;
  }

  async findByName(name: string) {
    const user = await this.photon.users.findOne({
      where: {
        name,
      },
    });

    return mapToUserEntity(user);
  }

  async findByEmail(email: string) {
    const user = await this.photon.users.findOne({
      where: {
        email,
      },
    });

    return mapToUserEntity(user);
  }

  async createUser({ email, name, password, role }: UserEntity) {
    const existingUser = await this.findByEmail(email);
    if (existingUser) {
      throw new Error(`email ${email} is already in use`);
    }

    const user = await this.photon.users.create({
      data: {
        email,
        name,
        password,
        ...(role === UserRole.USER && {
          profile: {
            create: {},
          },
        }),
      },
    });

    return mapToUserEntity(user);
  }

  async updateUser({ id, name, password, email, role }: UserEntity) {
    const existingUser = await this.findById(id);

    if (existingUser) {
      const updatedUser = await this.photon.users.update({
        where: { id },
        data: {
          name,
          password,
          email,
          role,
        },
      });

      return mapToUserEntity(updatedUser);
    } else {
      throw new Error(`User with id: ${id} not found`);
    }
  }

  async deleteUser(userId: string) {
    const user = await this.photon.users.findOne({
      where: {
        id: userId,
      },
      select: {
        profile: {
          select: {
            id: true,
          },
        },
      },
    });
    if (!user) return;

    await this.photon.userProfiles.delete({
      where: {
        id: user.profile.id,
      },
    });
    await this.photon.users.delete({
      where: { id: userId },
    });
  }
}

const mapToUserEntity = (data: User) => (data ? new UserEntity(data) : null);
