import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Parent, ResolveProperty } from '@nestjs/graphql';
import { CommentsRepository } from 'src/comments/comments.repository';
import { CurrentUser } from 'src/decorators/current-user.decorator';
import { CurrentUserModel } from 'src/decorators/current-user.model';
import { Roles } from 'src/decorators/roles.decorator';
import { GradesRepository } from 'src/grades/grades.repository';
import { GqlAuthGuard } from 'src/guards/gql.auth-guard';
import { RolesGuard } from 'src/guards/gql.role-guard';
import { PoemsRepository } from 'src/poems/poems.repository';
import { TagsRepository } from 'src/tags/tags.repository';
import { UserRole } from './enums/user-role';
import { DeleteUserInput } from './inputs/delete-user.input';
import { RegisterUserInput } from './inputs/register-user.input';
import { UpdateUserInput } from './inputs/update-user.input';
import { UserEntity } from './user.entity';
import { UsersService } from './users.service';
import { PoemEntity } from 'src/poems/poem.entity';

@Resolver(UserEntity)
export class UsersResolver {
  constructor(
    private readonly usersService: UsersService,
    private readonly tagsRepository: TagsRepository,
    private readonly poemsRepository: PoemsRepository,
    private readonly commentsRepository: CommentsRepository,
    private readonly gradesRepository: GradesRepository,
  ) {}

  @Query(() => [UserEntity])
  async users() {
    return await this.usersService.getUsers();
  }

  @Query(() => UserEntity)
  async getUser(@Args('id') userId: string) {
    return await this.usersService.getUserById(userId);
  }

  @Query(() => UserEntity)
  @UseGuards(GqlAuthGuard)
  async getUserDataFromJwt(@CurrentUser() user: CurrentUserModel) {
    return await this.usersService.getUserById(user.id);
  }

  @Mutation(() => Boolean)
  async registerUser(
    @Args('data') { email, name, password }: RegisterUserInput,
  ) {
    await this.usersService.createUser({
      email,
      name,
      password,
    });

    return true;
  }

  @Mutation(() => UserEntity)
  @UseGuards(GqlAuthGuard)
  async updateUser(
    @Args('data') { id, name, email, password, role }: UpdateUserInput,
  ) {
    const user = await this.usersService.updateUser({
      id,
      name,
      email,
      password,
      role,
    });

    return user;
  }

  @Mutation(() => Boolean)
  @UseGuards(GqlAuthGuard, RolesGuard)
  @Roles(UserRole.ADMIN)
  async deleteUser(@Args('data') { id }: DeleteUserInput) {
    await this.commentsRepository.deleteCommentsByAuthor(id);
    await this.poemsRepository.deletePoemsByAuthor(id);
    await this.tagsRepository.deleteTagsByAuthor(id);
    await this.gradesRepository.deleteGradesByAuthor(id);
    await this.usersService.deleteUser(id);

    return true;
  }

  @ResolveProperty(() => [PoemEntity])
  async poems(@Parent() { id }: UserEntity) {
    return await this.poemsRepository.findByAuthor(id);
  }
}
