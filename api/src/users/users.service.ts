import { Injectable } from '@nestjs/common';
import bcrypt from 'bcrypt';
import { UserEntity } from './user.entity';
import { UsersRepository } from './users.repository';

interface CreateUser {
  email: string;
  name: string;
  password: string;
}

@Injectable()
export class UsersService {
  constructor(
    private readonly usersRepository: UsersRepository,
  ) {}

  async getUsers(){
    return await this.usersRepository.findUsers();
  }

  async getUserById(userId: string) {
    return await this.usersRepository.findById(userId);
  }

  async getUserByName(name: string){
    return await this.usersRepository.findByName(name);
  }

  async createUser({ email, name, password }: CreateUser) {
    const userEntity = new UserEntity({
      email,
      name,
      password: await bcrypt.hash(password, 10),
    });

    return await this.usersRepository.createUser(userEntity);
  }

  async updateUser({ id, name, email, password, role }) {
    const userEntity = new UserEntity({
      id,
      name,
      email,
      password,
      role,
    });
    return await this.usersRepository.updateUser(userEntity);
  }

  async deleteUser(userId: string) {
    return await this.usersRepository.deleteUser(userId);
  }
}
