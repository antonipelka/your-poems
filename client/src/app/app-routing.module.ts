import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CopyrightComponent } from './components/copyright/copyright.component';
import { LoginComponent } from './components/login/login.component';
import { MainViewComponent } from './components/main-view/main-view.component';
import { MyPoemsComponent } from './components/my-poems/my-poems.component';
import { PoemViewComponent } from './components/poem-view/poem-view.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { SearchPoemsComponent } from './components/search-poems/search-poems.component';
import { TopPoemsComponent } from './components/top-poems/top-poems.component';
import { UserComponent } from './components/user/user.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: MainViewComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'users', component: UserComponent },
  { path: 'users/:id', component: MyPoemsComponent },
  { path: 'top', component: TopPoemsComponent },
  { path: 'search/:query', component: SearchPoemsComponent },
  { path: 'poems', component: MyPoemsComponent },
  { path: 'poems/:id', component: PoemViewComponent },
  { path: 'copyright-management', component: CopyrightComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
