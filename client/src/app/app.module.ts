import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule, MatButtonModule, MatCardModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatDialogRef, MatExpansionModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatNativeDateModule, MatPaginatorModule, MatRadioModule, MatSelectModule, MatSidenavModule, MatSnackBarModule, MatTableModule, MatToolbarModule, MatTooltipModule, MAT_DIALOG_DATA, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApolloModule } from 'apollo-angular';
import { HttpLinkModule } from 'apollo-angular-link-http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddCopyrightedPoemGQL } from './components/copyright/addCopyrightedPoemGQL';
import { CopyrightComponent } from './components/copyright/copyright.component';
import { GetCopyrightedPoems } from './components/copyright/getCopyrightedPoemsGQL';
import { TrainGQL } from './components/copyright/trainGQL';
import { LoginComponent } from './components/login/login.component';
import { AddCommentGQL } from './components/main-view/addCommentGQL';
import { AddPoemGQL } from './components/main-view/addPoemGQL';
import { AllPoemsGQL } from './components/main-view/allPoemsGQL';
import { DeleteCommentDialogComponent } from './components/main-view/delete-comment-dialog/delete-comment-dialog.component';
import { DeleteCommentGQL } from './components/main-view/deleteCommentGQL';
import { DeletePoemGQL } from './components/main-view/deletePoemGQL';
import { MainViewComponent } from './components/main-view/main-view.component';
import { PoemDialogComponent } from './components/main-view/poem-dialog/poem-dialog.component';
import { UpdatePoemGQL } from './components/main-view/updatePoemGQL';
import { DeletePoemDialogComponent } from './components/my-poems/delete-poem-dialog/delete-poem-dialog.component';
import { GetUserPoemsGQL } from './components/my-poems/getUserPoemsGQL';
import { MyPoemsComponent } from './components/my-poems/my-poems.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { GetPoemGQL } from './components/poem-view/getPoemGQL';
import { PoemViewComponent } from './components/poem-view/poem-view.component';
import { PoemsListComponent } from './components/poems-list/poems-list.component';
import { RegisterUserGQL } from './components/registration/registerUserGQL';
import { RegistrationComponent } from './components/registration/registration.component';
import { DeleteUserDialogComponent } from './components/user/delete-user-dialog/delete-user-dialog.component';
import { DeleteUserGQL } from './components/user/delete-user-dialog/deleteUserGQL';
import { GetUsersGQL } from './components/user/getUsersGQL';
import { UserComponent } from './components/user/user.component';
import { GraphQLModule } from './graphql.module';
import { LoginGQL } from './services/auth/LoginGQL';
import { RefreshTokenGQL } from './services/auth/RefreshTokenGQL';
import { ErrorComponent } from './components/error/error.component';
import { TopPoemsComponent } from './components/top-poems/top-poems.component';
import { TopPoemsGQL } from './components/top-poems/topPoemsGQL';
import { SearchPoemsComponent } from './components/search-poems/search-poems.component';
import { SearchPoemsGQL } from './components/search-poems/searchPoemsGQL';
import { ToggleLikeGQL } from './components/poem-view/toggleLikeGQL';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    MainViewComponent,
    PoemDialogComponent,
    UserComponent,
    MyPoemsComponent,
    DeletePoemDialogComponent,
    NavigationComponent,
    DeleteUserDialogComponent,
    DeleteCommentDialogComponent,
    PoemsListComponent,
    PoemViewComponent,
    CopyrightComponent,
    ErrorComponent,
    TopPoemsComponent,
    SearchPoemsComponent,
  ],
  imports: [
    HttpClientModule,
    ApolloModule,
    HttpLinkModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatBadgeModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatExpansionModule,
    GraphQLModule,
    HttpClientModule,
    MatDialogModule,
    MatSnackBarModule,
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 10000 } },
    AllPoemsGQL,
    AddPoemGQL,
    UpdatePoemGQL,
    DeletePoemGQL,
    RegisterUserGQL,
    AddCommentGQL,
    GetUsersGQL,
    LoginGQL,
    RefreshTokenGQL,
    DeleteUserGQL,
    DeleteCommentGQL,
    GetUserPoemsGQL,
    GetPoemGQL,
    TrainGQL,
    GetCopyrightedPoems,
    AddCopyrightedPoemGQL,
    TopPoemsGQL,
    SearchPoemsGQL,
    ToggleLikeGQL,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    PoemDialogComponent,
    DeletePoemDialogComponent,
    DeleteUserDialogComponent,
    DeleteCommentDialogComponent,
    ErrorComponent,
  ],
})
export class AppModule { }
