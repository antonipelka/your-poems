import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

export class AddCopyrightedPoemGQL extends Mutation {
  document = gql`
    mutation AddCopyrightedPoem($content: String!) {
      addCopyrightedPoem(content: $content)
    }
  `;
}