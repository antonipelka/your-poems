import { Component, OnInit } from '@angular/core';
import { TrainGQL } from './trainGQL';
import { take } from 'rxjs/operators';
import { GetCopyrightedPoems } from './getCopyrightedPoemsGQL';
import { AddCopyrightedPoemGQL } from './addCopyrightedPoemGQL';
import { MatSnackBar } from '@angular/material';
import { ErrorComponent } from '../error/error.component';

@Component({
  selector: 'app-copyright',
  templateUrl: './copyright.component.html',
  styleUrls: ['./copyright.component.css']
})
export class CopyrightComponent implements OnInit {

  content: string = '';
  copyrighted: string[] = [];

  constructor(
    private readonly trainGQL: TrainGQL,
    private readonly addCopyrightedContent: AddCopyrightedPoemGQL,
    private readonly getCopyrightedPoems: GetCopyrightedPoems,
    private readonly snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.getCopyrightedPoems.fetch().pipe(
      take(1),
    ).subscribe({
      next: ({
        data: {
          copyrightedPoems,
        },
      }) => this.copyrighted = copyrightedPoems.map(({ content }) => content),
    })
  }

  train() {
    this.trainGQL.mutate().pipe(take(1)).subscribe();
  }

  add() {
    if (this.content) {
      this.addCopyrightedContent
        .mutate({
          content: this.content,
        })
        .subscribe({
          next: () => {
            this.content = '';
            window.location.reload();
          },
          error: (error) => {
            console.error(error.graphQLErrors);
            this.snackBar.openFromComponent(ErrorComponent, {
              data: { errors: error.graphQLErrors },
            });
          }
        });
    }
  }

}
