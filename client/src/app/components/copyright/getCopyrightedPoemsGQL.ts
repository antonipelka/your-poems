import { Query } from 'apollo-angular';
import gql from 'graphql-tag';

export interface CopyrightedPoem {
  content: string;
}

export class GetCopyrightedPoems extends Query<Record<'copyrightedPoems', CopyrightedPoem[]>> {
  document = gql`
    query GetCopyrightedPoems {
      copyrightedPoems {
        content
      }
    }
  `;
}