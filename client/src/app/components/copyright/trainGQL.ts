import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

export class TrainGQL extends Mutation {
  document = gql`
    mutation Train {
      trainCopyrightModel
    }
  `;
}