import { Component, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  errors = [];

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) { }

  ngOnInit() {
    this.errors = this.data.errors.reduce((acc, { message }) => {
      const errors = Array.isArray(message.message)
        ? message.message.reduce(
          (acc, error) => acc.concat(Object.values(error.constraints)),
          [],
        ) : [typeof message.message === 'string' ? message.message : (message.error || 'Error')];
      return acc.concat(errors);
    }, []);
  }

}
