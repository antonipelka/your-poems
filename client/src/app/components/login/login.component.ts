import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ErrorComponent } from '../error/error.component';

const INITIAL_CREDENTIALS = {
  login: '',
  password: '',
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  credentials = { ...INITIAL_CREDENTIALS };

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
  ) {}

  login() {
    this.authService.login(this.credentials).subscribe({
      next: () => {
        this.credentials = { ...INITIAL_CREDENTIALS };
        this.router.navigate(['']);
      },
      error: (error) => {
        console.error(error.graphQLErrors);
        this.snackBar.openFromComponent(ErrorComponent, {
          data: { errors: error.graphQLErrors },
        });
      }
    });
  }
}
