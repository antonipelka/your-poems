import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Comment } from '../poem-view/getPoemGQL';

interface Response {
  createComment: Comment;
}

export class AddCommentGQL extends Mutation<Response> {
  document = gql`
    mutation createComment($content: String!, $poemId: String!) {
      createComment(data: {
        content: $content
        poemId: $poemId
      }) {
        id
        author {
          id
          name
        }
        content
      }
    }
  `;
}