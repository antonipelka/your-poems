import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

interface AddPoemResponse {
  createPoem: {
    id: string;
  };
}

export class AddPoemGQL extends Mutation<AddPoemResponse> {
  document = gql`
    mutation createPoem($title: String!, $content: String!, $tags: [String!]!) {
      createPoem(data: {
        title: $title
        content: $content
        tags: $tags
      }) {
        id
      }
    }
  `;
}
