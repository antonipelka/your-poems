import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Poem } from '../poem-view/getPoemGQL';

export interface Response {
  poems: Poem[];
}

export class AllPoemsGQL extends Query<Response> {
  document = gql`
    query Poems {
        poems {
          id
          title
          content
          createdAt
          likesCount
          grades {
            author {
              id
            }
          }
          author {
            id
            name
            email
          }
          comments {
            id
            author {
              id
              name
            }
            content            
          }
          tags {
            id
            name
          }
        }
      }
    `;
}