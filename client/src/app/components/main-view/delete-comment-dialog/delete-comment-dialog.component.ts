import { Component, OnInit, Inject } from '@angular/core';
import { DeleteCommentGQL } from '../deleteCommentGQL';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ErrorComponent } from '../../error/error.component';

@Component({
  selector: 'app-delete-comment-dialog',
  templateUrl: './delete-comment-dialog.component.html',
  styleUrls: ['./delete-comment-dialog.component.css']
})
export class DeleteCommentDialogComponent implements OnInit {

  commentId: string;

  constructor(
    private readonly dialogRef: MatDialogRef<DeleteCommentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly data: any,
    private readonly deleteCommentGQL: DeleteCommentGQL,
    private readonly snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.commentId = this.data.id;
  }

  deleteComment(): void {
    this.deleteCommentGQL
      .mutate({ id: this.commentId })
      .subscribe({
        next: () => this.dialogRef.close(true),
        error: (error) => {
          console.error(error.graphQLErrors);
          this.snackBar.openFromComponent(ErrorComponent, {
            data: { errors: error.graphQLErrors },
          });
        },
      });
  }


  onCancelClick(): void {
    this.dialogRef.close(false);
  }
}
