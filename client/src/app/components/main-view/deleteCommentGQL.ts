import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

export class DeleteCommentGQL extends Mutation {
  document = gql`
    mutation deleteComment($id: String!) {
      deleteComment(data: {
        id: $id
      })
    }
  `;
}