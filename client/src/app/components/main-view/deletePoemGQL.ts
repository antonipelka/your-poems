import {Injectable} from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

export class DeletePoemGQL extends Mutation {
  document = gql`
    mutation deletePoem($id: String!) {
      deletePoem(data: {
        id: $id
      })
    }
  `;
}