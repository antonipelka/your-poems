import { Component, OnInit } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Poem } from '../poem-view/getPoemGQL';
import { AllPoemsGQL } from './allPoemsGQL';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css'],
})
export class MainViewComponent implements OnInit {

  poems: (Poem & { commentContent: string })[] = [];

  constructor(private allPoemsGql: AllPoemsGQL) { }

  ngOnInit() {
    this.allPoemsGql.watch({}, {
      fetchPolicy: 'no-cache',
    })
      .valueChanges
      .pipe(
        take(1),
        map(result => result.data.poems.map((poem => ({ ...poem, commentContent: '' }))))
      ).subscribe({
        next: (poems) => this.poems = poems
      });
  }
}
