import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { AddPoemGQL } from '../addPoemGQL';
import { UpdatePoemGQL } from '../updatePoemGQL';
import { map } from 'rxjs/operators';
import { ErrorComponent } from '../../error/error.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-poem-dialog',
  templateUrl: './poem-dialog.component.html',
  styleUrls: ['./poem-dialog.component.css']
})
export class PoemDialogComponent implements OnInit {
  id: string;
  title: string;
  content: string;
  tags: string;

  constructor(
    private readonly dialogRef: MatDialogRef<PoemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly addPoemGQL: AddPoemGQL,
    private readonly updatePoemGQL: UpdatePoemGQL,
    private readonly snackBar: MatSnackBar,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    if (this.data && this.data.editFlag === true) {
      this.id = this.data.currentPoem.id;
      this.title = this.data.currentPoem.title;
      this.content = this.data.currentPoem.content;
      this.tags = this.data.currentPoem.tags.map(({ name }) => name).join(', ');
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const mutation = this.data && this.data.editFlag === true
      ? this.updatePoem()
      : this.addPoem();

    mutation.subscribe({
      next: ({ id }) => {
        this.router.navigate(['/poems', id]).then(() => {
          this.dialogRef.close();
        });
      },
      error: (error) => {
        console.error(error.graphQLErrors);
        this.snackBar.openFromComponent(ErrorComponent, {
          data: { errors: error.graphQLErrors },
        });
      },
    });
  }

  addPoem() {
    return this.addPoemGQL
      .mutate({
        title: this.title,
        content: this.content,
        tags: this.tags.split(',').map((tag) => tag.trim()),
      })
      .pipe(
        map(response => response.data.createPoem)
      );
  }

  updatePoem() {
    return this.updatePoemGQL
      .mutate({
        id: this.id,
        title: this.title,
        content: this.content,
        tags: this.tags.split(',').map((tag) => tag.trim()),
      })
      .pipe(
        map(response => response.data.updatePoem)
      );
  }
}
