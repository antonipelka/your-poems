import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

interface UpdatePoemArguments {
  id: string;
  title: string;
  content: string;
  tags: string[];
}

interface UpdatePoemResponse {
  updatePoem: {
    id: string;
  };
}

export class UpdatePoemGQL extends Mutation<UpdatePoemResponse, UpdatePoemArguments> {
  document = gql`
    mutation updatePoem($id: String!, $title: String!, $content: String!, $tags: [String!]!) {
      updatePoem(data: {
        id: $id
        title: $title
        content: $content
        tags: $tags
      }) {
        id
      }
    }
  `;
}