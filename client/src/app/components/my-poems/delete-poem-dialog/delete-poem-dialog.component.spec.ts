import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePoemDialogComponent } from './delete-poem-dialog.component';

describe('DeletePoemDialogComponent', () => {
  let component: DeletePoemDialogComponent;
  let fixture: ComponentFixture<DeletePoemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletePoemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePoemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
