 
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { DeletePoemGQL } from '../../main-view/deletePoemGQL';

@Component({
  selector: 'app-delete-poem-dialog',
  templateUrl: './delete-poem-dialog.component.html',
  styleUrls: ['./delete-poem-dialog.component.css']
})
export class DeletePoemDialogComponent implements OnInit {

  poemId: any;

  constructor(private dialogRef: MatDialogRef<DeletePoemDialogComponent>,
    private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
    private deletePoemGQL: DeletePoemGQL) { }

  ngOnInit() {
    this.poemId = this.data.id;
  }

  deletePoem(): void {
    console.log(this.poemId)
    this.deletePoemGQL
    .mutate({
      id: this.poemId.id
    })
    .subscribe(() => window.location.reload());
  }


  onCancelClick(): void {
      this.dialogRef.close(false);
  }

}
