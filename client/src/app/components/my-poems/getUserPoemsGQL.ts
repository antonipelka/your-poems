import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Poem, User } from '../poem-view/getPoemGQL';

export interface GetUserPoemsResponse extends User {
  poems: Poem[];
}

interface GetUserPoemsArguments {
  userId: string;
}

export class GetUserPoemsGQL extends Query<Record<'getUser', GetUserPoemsResponse>, GetUserPoemsArguments> {
  document = gql`
    query User($userId: String!) {
      getUser(id: $userId) {
        id
        name
        poems {
          id
          title
          content
          likesCount
          grades {
            author {
              id
            }
          }
          author {
            id
            name
            email
          }
          comments {
            id
            content
          }
          tags {
            id
            name
          }
        }
      }
    }
  `;
}
