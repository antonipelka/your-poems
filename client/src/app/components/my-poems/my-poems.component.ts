import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetUserPoemsGQL, GetUserPoemsResponse } from './getUserPoemsGQL';
import { take, switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-my-poems',
  templateUrl: './my-poems.component.html',
  styleUrls: ['./my-poems.component.css']
})
export class MyPoemsComponent implements OnInit {
  user: GetUserPoemsResponse = null;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly getUserPoemsGQL: GetUserPoemsGQL,
  ) { }

  ngOnInit() {
    this.route.params.pipe(
      take(1),
      switchMap(({ id }) => this.getUserPoemsGQL.fetch({
        userId: id,
      })),
      map(({ data: { getUser } }) => getUser),
    ).subscribe({
      next: (user: GetUserPoemsResponse) => {
        this.user = user;
      },
    });
  }
}
