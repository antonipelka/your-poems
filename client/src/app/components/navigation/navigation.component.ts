import { Component, OnInit } from '@angular/core';
import { AuthService, AuthenticatedUser } from 'src/app/services/auth/auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { PoemDialogComponent } from '../main-view/poem-dialog/poem-dialog.component';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  user$: Observable<AuthenticatedUser>;
  searchQuery: string = '';

  constructor(
    public readonly dialog: MatDialog,
    private readonly authService: AuthService,
    private readonly router: Router,
  ) {
    this.user$ = this.authService.user$;
  }

  ngOnInit() {}

  logout() {
    this.authService.logout();
    this.router.navigate(['']);
  }

  openDialog() {
    this.dialog.open(PoemDialogComponent, {
      width: '600px'
    });
  }

  search(event: KeyboardEvent) {
    this.router.navigate(['/search', this.searchQuery]);
    this.searchQuery = '';
  }
}
