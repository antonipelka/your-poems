import { Query } from 'apollo-angular';
import gql from 'graphql-tag';

interface Tag {
  id: string;
  name: string;
}

interface Grade {
  id: string;
  author: {
    id: string;
  };
}

export interface Poem {
  id: string;
  title: string;
  content: string;
  author: User;
  comments: Comment[];
  createdAt: string;
  posts: Poem[];
  tags: Tag[];
  likesCount: number;
  liked: boolean;
  grades: Grade[]
}

export interface Comment {
  id: string;
  author: User;
  content: string;
}

export interface Response {
  getPoem: Poem;
}

export interface User {
  id: string;
  name: string;
  email: string;
}

export class GetPoemGQL extends Query<Response> {
  document = gql`
    query GetPoem($id: String!) {
        getPoem(id: $id) {
          id
          title
          content
          createdAt
          likesCount
          grades {
            author {
              id
            }
          }
          author {
            id
            name
            email
          }
          comments {
            id
            author {
              id
              name
            }
            content            
          }
          tags {
            id
            name
          }
        }
      }
    `;
}