import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { AuthenticatedUser, AuthService } from 'src/app/services/auth/auth.service';
import { ErrorComponent } from '../error/error.component';
import { AddCommentGQL } from '../main-view/addCommentGQL';
import { DeleteCommentDialogComponent } from '../main-view/delete-comment-dialog/delete-comment-dialog.component';
import { PoemDialogComponent } from '../main-view/poem-dialog/poem-dialog.component';
import { DeletePoemDialogComponent } from '../my-poems/delete-poem-dialog/delete-poem-dialog.component';
import { GetPoemGQL, Poem } from './getPoemGQL';
import { ToggleLikeGQL } from './toggleLikeGQL';

interface PoemWithEditableComment extends Poem {
  commentContent: string;
}

@Component({
  selector: 'app-poem-view',
  templateUrl: './poem-view.component.html',
  styleUrls: ['./poem-view.component.css']
})
export class PoemViewComponent implements OnInit {
  user$: Observable<AuthenticatedUser>;
  @Input() poem: PoemWithEditableComment;
  @Input() showComments = true;

  encodedURL = '';

  constructor(
    private readonly dialog: MatDialog,
    private readonly addCommentGQL: AddCommentGQL,
    private readonly getPoemGQL: GetPoemGQL,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
    private readonly authService: AuthService,
    private readonly toggleLikeGQL: ToggleLikeGQL,
  ) {
    this.user$ = this.authService.user$;
  }

  ngOnInit() {
    if (!this.poem) {
      this.loadPoem();
    } else {
      this.setLink();
    }
  }

  loadPoem() {
    this.route.params.pipe(
      take(1),
      switchMap(({ id }) => {
        if (!id) {
          return of({
            data: {
              getPoem: this.poem,
            }
          });
        }
        return this.getPoemGQL.fetch({
          id,
        }, {
          fetchPolicy: 'no-cache',
        });
      }),
      map(({ data: { getPoem } }) => getPoem),
    ).subscribe({
      next: (poem) => {
        this.poem = { ...poem, commentContent: '' };
        this.setLink();
      },
    });
  }

  setLink() {
    this.user$.subscribe({
      next: ({ id }) => {
        this.poem.liked = !!this.poem.grades.find(({ author }) => author.id === id);
      }
    });
    this.encodedURL = encodeURIComponent('https://yourpoems.com' + this.router.createUrlTree(['/poems', this.poem.id]).toString());
  }

  donate(poem: Poem) {
    const email = poem.author.email;
    window.open(`https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=${encodeURIComponent(email)}&item_name=Darowizna%20(${poem.id})&currency_code=USD&source=${window.location.href}`);
  }

  openAddPoemDialog() {
    this.dialog.open(PoemDialogComponent, {
      width: '600px'
    });
  }

  openEditPoemDialog(poem: Poem) {
    const dialogRef = this.dialog.open(PoemDialogComponent, {
      width: '600px',
      data: {
        currentPoem: poem,
        editFlag: true,
      },
    });
    dialogRef.afterClosed().subscribe(() => this.loadPoem());
  }

  openDeletePoemDialog(id: string) {
    let dialogRef = this.dialog.open(DeletePoemDialogComponent, {
      width: '600px',
      data: {
        id
      }
    });
    dialogRef.afterClosed().subscribe(() => this.router.navigate(['/']));
  }


  addComment(poem: PoemWithEditableComment) {
    const { commentContent: content } = this.poem;
    this.addCommentGQL
      .mutate({
        content,
        poemId: poem.id,
      })
      .subscribe({
        next: ({ data: { createComment } }) => {
          poem.commentContent = '';
          poem.comments.unshift(createComment);
        },
        error: (error) => {
          console.error(error.graphQLErrors);
          this.snackBar.openFromComponent(ErrorComponent, {
            data: { errors: error.graphQLErrors },
          });
        },
      });
  }

  deleteComment(poem: Poem, commentId: string) {
    const dialogRef = this.dialog.open(DeleteCommentDialogComponent, {
      width: '600px',
      data: {
        id: commentId,
      },
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        poem.comments = poem.comments.filter(({ id }) => id !== commentId);
      }
    });
  }

  toggleLike(poem: Poem) {
    this.toggleLikeGQL.mutate({
      poemId: poem.id,
    }).subscribe({
      next: ({ data: { toggleLike: { liked, likesCount } } }) => {
        poem.liked = liked;
        poem.likesCount = likesCount;
      },
      error: (error) => {
        console.error(error.graphQLErrors);
        this.snackBar.openFromComponent(ErrorComponent, {
          data: { errors: error.graphQLErrors },
        });
      },
    })
  }
}
