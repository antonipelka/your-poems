import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

interface Response {
  toggleLike: {
    poemId: string;
    liked: boolean;
    likesCount: number;
  };
}

export class ToggleLikeGQL extends Mutation<Response> {
  document = gql`
    mutation ToggleLike($poemId: String!) {
      toggleLike(poemId: $poemId) {
        poemId
        liked
        likesCount
      }
    }
  `;
}