import { Component, Input } from '@angular/core';
import { Poem } from '../poem-view/getPoemGQL';

interface PoemWithEditableComment extends Poem {
  commentContent: string;
}

@Component({
  selector: 'app-poems-list',
  templateUrl: './poems-list.component.html',
  styleUrls: ['./poems-list.component.css']
})
export class PoemsListComponent {
  @Input() title = 'Poems';
  @Input() poems: PoemWithEditableComment[] = [];
}
