import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

interface RegisterArguments {
  email: string;
  name: string;
  password: string;
}

export class RegisterUserGQL extends Mutation<boolean, RegisterArguments> {
  document = gql`
    mutation registerUser($email: String!, $name: String!, $password: String!) {
      registerUser(data: {
        email: $email
        name: $name,
        password: $password
      })
    }
  `;
}