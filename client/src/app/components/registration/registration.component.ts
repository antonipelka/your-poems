import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ErrorComponent } from '../error/error.component';
import { RegisterUserGQL } from './registerUserGQL';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {

  email: string = '';
  name: string = '';
  password: string = '';

  constructor(
    private readonly registerUser: RegisterUserGQL,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
  ) { }

  addUser() {
    this.registerUser
      .mutate({
        email: this.email,
        name: this.name,
        password: this.password
      })
      .subscribe({
        next: () => {
          this.router.navigate(['/login']);
        },
        error: (error) => {
          console.error(error.graphQLErrors);
          this.snackBar.openFromComponent(ErrorComponent, {
            data: { errors: error.graphQLErrors },
          });
        }
      });
  }
}
