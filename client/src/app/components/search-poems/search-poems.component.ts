import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { Poem } from '../poem-view/getPoemGQL';
import { SearchPoemsGQL } from './searchPoemsGQL';

@Component({
  selector: 'app-search-poems',
  templateUrl: './search-poems.component.html',
  styleUrls: ['./search-poems.component.css']
})
export class SearchPoemsComponent implements OnInit {
  poems: Poem[] = [];
  query: string = '';

  constructor(
    private readonly searchPoemsGQL: SearchPoemsGQL,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.pipe(
      switchMap(({ query }) => {
        this.query = query;
        return this.searchPoemsGQL.watch({ query }, { fetchPolicy: 'no-cache' }).valueChanges;
      }),
      map(result => result.data.searchPoems.map((poem => ({ ...poem, commentContent: '' })))),
    ).subscribe({
      next: (poems) => this.poems = poems,
    });
  }

}
