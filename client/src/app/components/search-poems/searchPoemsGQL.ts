import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Poem } from '../poem-view/getPoemGQL';

export interface Response {
  searchPoems: Poem[];
}

export class SearchPoemsGQL extends Query<Response> {
  document = gql`
    query SearchPoems($query: String!) {
      searchPoems(query: $query) {
        id
        title
        content
        createdAt
        likesCount
        grades {
          author {
            id
          }
        }
        author {
          id
          name
          email
        }
        comments {
          id
          author {
            id
            name
          }
          content            
        }
        tags {
          id
          name
        }
      }
    }
  `;
}