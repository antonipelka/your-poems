import { Component, OnInit } from '@angular/core';
import { take, map } from 'rxjs/operators';
import { TopPoemsGQL } from './topPoemsGQL';
import { Poem } from '../poem-view/getPoemGQL';

@Component({
  selector: 'app-top-poems',
  templateUrl: './top-poems.component.html',
  styleUrls: ['./top-poems.component.css']
})
export class TopPoemsComponent implements OnInit {
  poems: Poem[] = [];

  constructor(
    private readonly topPoemsGql: TopPoemsGQL,
  ) { }

  ngOnInit() {
    this.topPoemsGql.watch({}, {
      fetchPolicy: 'no-cache',
    })
      .valueChanges
      .pipe(
        take(1),
        map(result => result.data.topPoems.map((poem => ({ ...poem, commentContent: '' }))))
      ).subscribe({
        next: (poems) => this.poems = poems
      });
  }

}
