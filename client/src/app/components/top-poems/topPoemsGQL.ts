import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Poem } from '../poem-view/getPoemGQL';

export interface Response {
  topPoems: Poem[];
}

export class TopPoemsGQL extends Query<Response> {
  document = gql`
    query TopPoems {
      topPoems {
        id
        title
        content
        createdAt
        likesCount
        grades {
          author {
            id
          }
        }
        author {
          id
          name
          email
        }
        comments {
          id
          author {
            id
            name
          }
          content            
        }
        tags {
          id
          name
        }
      }
    }
  `;
}