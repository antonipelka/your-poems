import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { DeleteUserGQL } from './deleteUserGQL';

@Component({
  selector: 'app-delete-user-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.css']
})
export class DeleteUserDialogComponent implements OnInit {

  userId: string;

  constructor(private dialogRef: MatDialogRef<DeleteUserDialogComponent>,
    private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
    private deleteUserGQL: DeleteUserGQL) { }

  ngOnInit() {
    this.userId = this.data.id;
  }

  deleteUser(): void {
    this.deleteUserGQL
      .mutate({
        id: this.userId
      })
      .subscribe(() => {
        this.dialogRef.close(this.userId);
      });
  }


  onCancelClick(): void {
    this.dialogRef.close(false);
  }

}
