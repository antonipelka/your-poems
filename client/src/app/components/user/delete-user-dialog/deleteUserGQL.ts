import {Injectable} from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

export class DeleteUserGQL extends Mutation {
  document = gql`
    mutation deleteUser($id: String!) {
      deleteUser(data: {
        id: $id
      })
    }
  `;
}