import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { User } from '../poem-view/getPoemGQL';

export interface Response {
  users: User[];
}

export class GetUsersGQL extends Query<Response> {
  document = gql`
    query Users {
      users {
        id
        name
        email
      }
    }
     `;
}