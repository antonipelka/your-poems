import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticatedUser, AuthService } from 'src/app/services/auth/auth.service';
import { DeleteUserDialogComponent } from './delete-user-dialog/delete-user-dialog.component';
import { GetUsersGQL } from './getUsersGQL';
import { User } from '../poem-view/getPoemGQL';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user$: Observable<AuthenticatedUser>;
  users: User[];
  displayedColumns: string[] = ['name', 'email', 'actions'];

  constructor(
    private dialog: MatDialog,
    private getUsersGQL: GetUsersGQL,
    private readonly authService: AuthService,
  ) {
    this.user$ = this.authService.user$;
  }

  ngOnInit() {
    this.getUsersGQL.watch()
      .valueChanges
      .pipe(
        map(result => result.data.users)
      ).subscribe((users) => {
        this.users = users;
      });
  }

  deleteUser(id: string) {
    let dialogRef = this.dialog.open(DeleteUserDialogComponent, {
      width: '600px',
      data: {
        id
      }
    });
    dialogRef.afterClosed().subscribe(deletedId => {
      this.users = this.users.filter(({ id }) => id !== deletedId );
    });
  }

}
