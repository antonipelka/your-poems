import { Mutation, } from 'apollo-angular';
import gql from 'graphql-tag';
import { TokenResponse } from './TokenResponse';

interface LoginArguments {
  login: string;
  password: string;
}

export class LoginGQL extends Mutation<Record<'login', TokenResponse>, LoginArguments> {
  document = gql`
    mutation Login($login: String!, $password: String!) {
      login(data: {
        login: $login
        password: $password
      }) {
        token
        user {
          id
          name
          role
        }
      }
    }
  `;
}