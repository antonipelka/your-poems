import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { TokenResponse } from './TokenResponse';

export class RefreshTokenGQL extends Mutation<Record<'refreshToken', TokenResponse>> {
  document = gql`
    mutation RefreshToken {
      refreshToken {
        token
        user {
          id
          name
          role
        }
      }
    }
  `;
}