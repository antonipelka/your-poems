export interface TokenResponse {
  token: string;
  user: {
    id: string;
    name: string;
    role: 'USER' | 'ADMIN';
  };
}
