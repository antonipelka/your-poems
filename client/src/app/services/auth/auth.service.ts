import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { LoginGQL } from './LoginGQL';
import { RefreshTokenGQL } from './RefreshTokenGQL';
import { TokenResponse } from "./TokenResponse";

interface LoginCommand {
  login: string;
  password: string;
}

export interface AuthenticatedUser {
  id: string;
  name: string;
  role: 'USER' | 'ADMIN';
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  authenticated$ = new BehaviorSubject(false);
  user$ = new BehaviorSubject<AuthenticatedUser>(null);

  constructor(
    private readonly loginGQL: LoginGQL,
    private readonly refreshTokenGQL: RefreshTokenGQL, 
  ) {
    this.checkToken();
  }

  private checkToken() {
    const token = localStorage.getItem('token');

    if (token) {
      this.refreshTokenGQL.mutate().pipe(take(1)).subscribe({
        next: ({ data: { refreshToken} }) => {
          this.authenticate(refreshToken);
        },
        error: () => {
          this.logout();
        },
      });
    }
  }

  private authenticate({ token, user }: TokenResponse) {
    localStorage.setItem('token', token);
    this.authenticated$.next(true);
    this.user$.next(user);
  }

  login({ login, password }: LoginCommand) {
    return this.loginGQL.mutate({
      login,
      password,
    }).pipe(
      take(1),
      map(({ data: { login } }) => {
        this.authenticate(login);
      }),
    );
  }

  logout() {
    localStorage.removeItem('token');
    this.authenticated$.next(false);
    this.user$.next(null);
  }
}
