from flask import Flask, request, Response
from tensorflow import keras
from keras_preprocessing.text import tokenizer_from_json
import json
from io import open
from os import path
from train import train_model

app = Flask(__name__)

# load previous model
if path.isfile('tokenizer.json'):
  with open('tokenizer.json') as handle:
    data = json.load(handle)
    tokenizer = tokenizer_from_json(data)
if path.isfile('copyright.h5'):
  model = keras.models.load_model('copyright.h5')

@app.route('/train', methods=['POST'])
def train():
  global model, tokenizer
  model, tokenizer = train_model()
  return Response(status=204)

@app.route('/check', methods=['POST'])
def check():
  try:
    content = request.json['content']
    tokenized = tokenizer.texts_to_sequences([content])
    padded = keras.preprocessing.sequence.pad_sequences(
      tokenized,
      maxlen=100,
      padding='post',
      value=0
    )
    prediction = model.predict(padded)
    probability = prediction.max()
    copyright = True if probability > 0.8 else False

    truncated = (content[:25] + '..') if len(content) > 25 else content
    print(f'Copyright check for "{truncated}" resulted with {copyright} ({probability})')

    return {
      'copyright': copyright
    }
  except:
    return {
      'copyright': False
    }
