from __future__ import absolute_import, division, print_function, unicode_literals
from tensorflow import keras
from io import open
import json
import numpy as np
from requests import get
import tensorflow as tf

def load_texts():
  response = get('http://api:3000/copyright/data')
  return np.array(response.json())

def train_model():
  train_texts = load_texts()
  output_size = 3
  # one hot encoded with round robin
  train_labels = np.empty((0, output_size))
  for text_index in range(train_texts.size):
    sparse_label = [0 for _ in range(output_size)] # set cold
    sparse_label[text_index % output_size] = 1 # set hot
    train_labels = np.append(train_labels, [sparse_label], axis=0)

  tokenizer = keras.preprocessing.text.Tokenizer()
  tokenizer.fit_on_texts(train_texts)

  word_count = len(tokenizer.word_index) + 1 # vocabulary + padding

  train_sequences = tokenizer.texts_to_sequences(train_texts)
  padded_train = keras.preprocessing.sequence.pad_sequences(train_sequences, maxlen=100, padding='post', value=0)

  # model architecture
  model = keras.Sequential([
    keras.layers.Embedding(input_dim=word_count, output_dim=32, mask_zero=True),
    keras.layers.LSTM(32, activation='relu'),
    keras.layers.Dense(16, activation='relu'),
    keras.layers.Dense(output_size, activation='sigmoid')
  ])
  model.summary()
  model.compile(optimized='adam', loss='binary_crossentropy', metrics=['accuracy'])

  # model training
  model.fit(
    padded_train,
    train_labels,
    epochs=420,
  )

  # save new model
  model.save('copyright.h5')
  tokenizer_json = tokenizer.to_json()
  with open('tokenizer.json', 'w', encoding='utf-8') as handle:
      handle.write(json.dumps(tokenizer_json, ensure_ascii=False))
  print('Saved new model!')

  return model, tokenizer
